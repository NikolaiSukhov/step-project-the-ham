const tabsBtn = document.querySelectorAll('.tb1');
const tabsItem = document.querySelectorAll('.txt');
const tabsImg = document.querySelectorAll('.img-tabs');

tabsBtn.forEach(function (e, index) {
  e.addEventListener('click', function () {
    const currentBtn = e;
    const tabID = currentBtn.getAttribute('data-tab');
    const currentTab = document.querySelector(tabID);
    const currentImg = tabsImg[index];

    tabsBtn.forEach(function (e) {
      e.classList.remove('active');
    });

    tabsItem.forEach(function (e) {
      e.classList.remove('active');
    });

    tabsImg.forEach(function (e) {
      e.classList.remove('active');
    });

    currentBtn.classList.add('active');
    currentTab.classList.add('active');
    currentImg.classList.add('active');
  });
});

function allimages(){
 const   allBtn = document.querySelector('.tb1d')
const allImg = document.querySelectorAll('.all')
allBtn.addEventListener('click', function(e){
    allImg.forEach(function (e) {
        e.classList.remove('active');
       e.classList.add('active');
      });  
})
}
allimages()

function designBtn () {
const landing = document.querySelectorAll('.image-holder.landing')
    const web = document.querySelectorAll('.image-holder.web')
    const wordpress = document.querySelectorAll('.image-holder.press')
    const deisgnBtnclick = document.querySelector('.tb1d-design')
    const designImgs = document.querySelectorAll('.image-holder.design')
    deisgnBtnclick.addEventListener('click', function(e){
        landing.forEach(function (e) {
            e.classList.remove('active');
          });
          web.forEach(function (e) {
            e.classList.remove('active');
          });
          wordpress.forEach(function (e) {
            e.classList.remove('active');
          });
        designImgs.forEach(function (e) {
            e.classList.add('active');
          });
    })

    console.log(deisgnBtnclick);
console.log(designImgs);

}
designBtn ()
function webBtn () {
    const webButton =document.querySelector('.tb1d-web')
    const landing = document.querySelectorAll('.image-holder.landing')
        const web = document.querySelectorAll('.image-holder.web')
        const wordpress = document.querySelectorAll('.image-holder.press')
        const deisgnBtnclick = document.querySelector('.tb1d-design')
        const designImgs = document.querySelectorAll('.image-holder.design')
        webButton.addEventListener('click', function(e){
            landing.forEach(function (e) {
                e.classList.remove('active');
              });
              web.forEach(function (e) {
                e.classList.add('active');
              });
              wordpress.forEach(function (e) {
                e.classList.remove('active');
              });
            designImgs.forEach(function (e) {
                e.classList.remove('active');
              });
        })
    
        console.log(deisgnBtnclick);
    console.log(designImgs);
    
    }
    webBtn ()
    function landingBtn () {
        const landingButton = document.querySelector ('.tb1d-landing')
        const landing = document.querySelectorAll('.image-holder.landing')
            const web = document.querySelectorAll('.image-holder.web')
            const wordpress = document.querySelectorAll('.image-holder.press')
            const deisgnBtnclick = document.querySelector('.tb1d-design')
            const designImgs = document.querySelectorAll('.image-holder.design')
            landingButton.addEventListener('click', function(e){
                landing.forEach(function (e) {
                    e.classList.remove('active');
                  });
                  web.forEach(function (e) {
                    e.classList.remove('active');
                  });
                  wordpress.forEach(function (e) {
                    e.classList.remove('active');
                  });
                designImgs.forEach(function (e) {
                    e.classList.add('active');
                  });
            })
        
            console.log(deisgnBtnclick);
        console.log(designImgs);
        
        }
        landingBtn ()
        function wordpressBtn () {
            const wpButton = document.querySelector ('.tb1d-press')
            const landing = document.querySelectorAll('.image-holder.landing')
                const web = document.querySelectorAll('.image-holder.web')
                const wordpress = document.querySelectorAll('.image-holder.press')
                const deisgnBtnclick = document.querySelector('.tb1d-design')
                const designImgs = document.querySelectorAll('.image-holder.design')
                wpButton.addEventListener('click', function(e){
                    landing.forEach(function (e) {
                        e.classList.remove('active');
                      });
                      web.forEach(function (e) {
                        e.classList.remove('active');
                      });
                      wordpress.forEach(function (e) {
                        e.classList.add('active');
                      });
                    designImgs.forEach(function (e) {
                        e.classList.remove('active');
                      });
                })
            
                console.log(deisgnBtnclick);
            console.log(designImgs);
            
            }
            wordpressBtn ()
            
            function activateUnactive() {
                const btnUnderTabs = document.querySelector('.btn-under-tabs');
                const unactiveElements = document.querySelectorAll('.unactive');
                
                btnUnderTabs.addEventListener('click', () => {
                  unactiveElements.forEach(el => {
                    el.classList.add('active');
                    btnUnderTabs.style.display = 'none';
                  });
                });
              }
              
              
              activateUnactive();

              const btnPrev = document.querySelector('.btn-crsl');
const btnNext = document.querySelector('.btn-corusel-2');
const names = document.querySelectorAll('.names-corusel');
const professions = document.querySelectorAll('.profession-corusel');
const images = document.querySelectorAll('.img-corusel-1');
const texts = document.querySelectorAll('.text-for-corusel');
const items = document.querySelectorAll('.corusel-w');

let activeIndex = 0;

btnPrev.addEventListener('click', () => {
  if (activeIndex === 0) {
    activeIndex = images.length - 1;
  } else {
    activeIndex--;
  }

  updateActiveElements();
});

btnNext.addEventListener('click', () => {
  if (activeIndex === images.length - 1) {
    activeIndex = 0;
  } else {
    activeIndex++;
  }

  updateActiveElements();
});

function updateActiveElements() {
  for (let i = 0; i < images.length; i++) {
    images[i].classList.remove('active');
    items[i].classList.remove('active');
    names[i].classList.remove('active');
    professions[i].classList.remove('active');
    texts[i].classList.remove('active');
  }

  images[activeIndex].classList.add('active');
  items[activeIndex].classList.add('active');
  names[activeIndex].classList.add('active');
  professions[activeIndex].classList.add('active');
  texts[activeIndex].classList.add('active');
}




images.forEach(image => {
  image.addEventListener('click', () => {
    const index = image.dataset.index;
    images.forEach(image => image.classList.remove('active'));
    names.forEach(name => name.classList.remove('active'));
    professions.forEach(profession => profession.classList.remove('active'));
    image.classList.add('active');
    names[index].classList.add('active');
    professions[index].classList.add('active');
  });
});

